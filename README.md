# Varsion 1.0.1

# Overview

Modifier method that allows you to concatenate current campaign experience with the experiences has been already seen by visitor accross the site. So that concatenated experiences will be available as `data.campaignInfo`.

__Note:__ Modifier creates another `data` property called `currentCampaignInfo` that contains current campaign experience. 

__sitePersist(data)__

Parameters:

Name        | Type   | Description
:----------:| :----: | :--------------: |
data        | Object | Campaign Data    |

# Download

[site-persist.js](https://bitbucket.org/mm-global-se/site-persist/src/master/src/site-persist.min.js)

#Deployment Details

* Take modifier method code from [here](https://bitbucket.org/mm-global-se/site-persist/src/master/src/site-persist.min.js)

* Add it to `defaults.modifiers` object inside integration registration method

* Update config object to match the requirements:

    1. `maxDataSize` - every 3d party has its own restrictions on how many characters it accepts

    2. `cookieName` - `mm-if-site-persist` by default; change it if you want to use another cookie name for storing campaigns visitor saw during his jorney throught the site

_Example_:

```javascript

mmcore.IntegrationFactory.register('Google Analytics', {
  defaults: {
    modifieres: {
      sitePersist: function (data) {
        // ...
      }
    },
  },

  validate: ...,

  check: ...,

  exec: ...

  // ...
});

```