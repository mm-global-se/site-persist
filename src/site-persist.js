// version 1.0.1
var modifiers = {
    sitePersist: function (data) {
        /**
         * modifier configuration
         * Please adapt to match your case
         */
        var config = {
            // allowed amount of characters to be sent to 3d party
            maxDataSize: Infinity,
            // cookie name for campaign data storage
            cookieName: 'mm-if-site-persist'
        },

        /**
         * get campaign index in stored campaigns array
         * @param  {Array}  dataArr
         * @return {Number} index of item in `dataArr` or '-1'
         */
        getCampaignIndex = function (dataArr) {
            var i;

            for (i = dataArr.length; i --;) {
                if (new RegExp('(' + data.campaign + ')' + '\=').test(dataArr[i])) {
                    return i;
                }
            }

            return -1;
        },

        /**
         * Concatinate current campaign with stored ones
         * @return {[type]} [description]
         */
        concatCampaigns = function () {
            var storedInfo = getFromStorage(config.cookieName),
                parsedInfo = storedInfo ? JSON.parse(storedInfo) : [],
                i = getCampaignIndex(parsedInfo)

            if (i !== -1) {
                parsedInfo[i] = data.campaignInfo;
            } else {
                parsedInfo.push(data.campaignInfo);
            }

            return parsedInfo.join('&');
        },

        /**
         * Get stored campaigns
         * @return {String}
         */
        getFromStorage = function () {
            return mmcore.GetCookie(config.cookieName, 1);
        },

        /**
         * Save updated campaigns to storage
         * @param  {String} dataArr
         * @return {void}
         */
        saveToStorage = function (concatData) {
            var concatDataArr = concatData.split('&');
            mmcore.SetCookie(config.cookieName, JSON.stringify(concatDataArr), 365, 1);
        },

        /**
         * Remove oldest campaigns to match maxDataSize
         * @param  {String} concatData
         * @return {String}
         */
        removeOldestCampaigns = function (concatData) {
            var parsedData = concatData.split('&');

            while (true) {
                if (parsedData.join('&').length >= config.maxDataSize) {
                    parsedData.shift();
                } else {
                    return parsedData.join('&');
                }
            }
        },

        /**
         * Update Campaign Info to be sent to 3d party
         * @param  {String} concatData
         * @return {void}
         */
        updateCampaignInfo = function (concatData) {
            data.currentCampaignInfo = data.campaignInfo;
            data.campaignInfo = concatData;
        },

        // initialize
        initialize = (function () {
            // `concatData` here contains information about
            // all campaigns from the storage + current campaign
            var concatData = concatCampaigns();

            // `concatData` here contains campaigns' information in allowed range (120 chars)
            // (older campaigns will be removed in order to keep MAX. 120 chars)
            concatData = removeOldestCampaigns(concatData);

            // 1. `data.campaignInfo` is updated to contain concatenated campaigns
            // 2. new property `data.currentCampaignInfo` is created to contain current campaign info
            //    in case we need to send it to separatly
            updateCampaignInfo(concatData);

            // save updated campaigns' information to storage
            saveToStorage(concatData);
        })();
    }
};